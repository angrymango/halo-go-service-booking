/**
 * View Models used by Spring MVC REST controllers.
 */
package au.com.halogo.booking.web.rest.vm;
